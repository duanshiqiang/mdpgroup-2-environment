package mdp.robot;

import mdp.environment.Environment;

public class MDPRobot {
	private Environment env;
	
	public MDPRobot(Environment env) {
		this.env = env;
	}
	
	public static void main(String[] args) {
		Environment env = new Environment();
		env.setup(true, false);
		RobotAndroidResponse res = new RobotAndroidResponse(env);
		new Thread(res).run();
	}
}
