package mdp.robot;

import java.util.concurrent.BlockingQueue;

import mdp.environment.Environment;
import mdp.environment.MDPAndroidRequest;

public class RobotAndroidResponse implements Runnable {
	
	private Environment env;
	
	public RobotAndroidResponse(Environment env){
		this.env = env;
	}
	public void run(){
		while(true) {
			MDPAndroidRequest req = null;
			try {
				req = env.androidRequestQueue.take();
				System.out.println(req.getData());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
