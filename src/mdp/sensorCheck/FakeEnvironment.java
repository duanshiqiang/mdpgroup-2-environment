package mdp.sensorCheck;

import java.util.Random;

import mdp.environment.Environment;
import mdp.environment.MDPSensorData;

public class FakeEnvironment extends Environment {
	public FakeEnvironment() {
		super();
	}
	
	@Override
	public MDPSensorData getSensorData() {
		MDPSensorData ret = new MDPSensorData();
		ret.distanceSensor1 = new Random().nextInt(100);
		ret.distanceSensor2 = new Random().nextInt(100);
		ret.distanceSensor3 = new Random().nextInt(100);
		return ret;
	}
}
