package mdp.sensorCheck;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.Timer;

import mdp.environment.Environment;
import mdp.environment.MDPSensorData;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.RefineryUtilities;

public class MDPSensorDemo extends ApplicationFrame {
	private Environment env;
	
	/**
	 * The string could be a sensor ID
	 */
	private HashMap<String, TimeSeries> sensorTimeSeries;
	
	public MDPSensorDemo(String title, Environment env) {
		super(title);
		this.env = env;
		this.sensorTimeSeries = new HashMap<String, TimeSeries>();
		
		// Create sensorTimeSeries
		TimeSeriesCollection dataset = this.createSensorTimeSeries();
		
		DateAxis domain = new DateAxis("Time");
		NumberAxis range = new NumberAxis("Value");
		domain.setTickLabelFont(new Font("SansSerif", Font.PLAIN, 12));
		range.setTickLabelFont(new Font("SansSerif", Font.PLAIN, 12));
		domain.setLabelFont(new Font("SansSerif", Font.PLAIN, 14));
		range.setLabelFont(new Font("SansSerif", Font.PLAIN, 14));
		
		XYItemRenderer renderer = new XYLineAndShapeRenderer(true, false);
		renderer.setSeriesPaint(0, Color.red);
		renderer.setSeriesPaint(1, Color.green);
		renderer.setSeriesPaint(2, Color.black);
		renderer.setSeriesPaint(3, Color.cyan);
		//renderer.setStroke(new BasicStroke(3f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
		XYPlot plot = new XYPlot(dataset, domain, range, renderer);
		plot.setBackgroundPaint(Color.lightGray);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		domain.setAutoRange(true);
		domain.setLowerMargin(0.0);
		domain.setUpperMargin(0.0);
		domain.setTickLabelsVisible(true);
		range.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		
		JFreeChart chart = new JFreeChart(title,
				new Font("SansSerif", Font.BOLD, 24), plot, true);
		chart.setBackgroundPaint(Color.white);
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setBorder(BorderFactory.createCompoundBorder(
								BorderFactory.createEmptyBorder(4, 4, 4, 4),
								BorderFactory.createLineBorder(Color.black)) );
		chartPanel.setPreferredSize(new java.awt.Dimension(1024, 576));
		setContentPane(chartPanel);
		
	}
	
	private TimeSeriesCollection createSensorTimeSeries() {
		this.sensorTimeSeries.put("distanceSensor1", new TimeSeries("distanceSensor1", Millisecond.class));
		this.sensorTimeSeries.put("distanceSensor2", new TimeSeries("distanceSensor2", Millisecond.class));
		this.sensorTimeSeries.put("distanceSensor3", new TimeSeries("distanceSensor3", Millisecond.class));
		this.sensorTimeSeries.put("distanceSensor4", new TimeSeries("distanceSensor4", Millisecond.class));
		
		this.sensorTimeSeries.get("distanceSensor1").setMaximumItemAge(4000);
		this.sensorTimeSeries.get("distanceSensor2").setMaximumItemAge(4000);
		this.sensorTimeSeries.get("distanceSensor3").setMaximumItemAge(4000);
		this.sensorTimeSeries.get("distanceSensor4").setMaximumItemAge(4000);
		
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(this.sensorTimeSeries.get("distanceSensor1"));
		dataset.addSeries(this.sensorTimeSeries.get("distanceSensor2"));
		dataset.addSeries(this.sensorTimeSeries.get("distanceSensor3"));
		dataset.addSeries(this.sensorTimeSeries.get("distanceSensor4"));
		return dataset;
	}
	
	private void updateSensorData(MDPSensorData sensorData) {
		this.sensorTimeSeries.get("distanceSensor1").add(new Millisecond(), sensorData.distanceSensor1);
		this.sensorTimeSeries.get("distanceSensor2").add(new Millisecond(), sensorData.distanceSensor2);
		this.sensorTimeSeries.get("distanceSensor3").add(new Millisecond(), sensorData.distanceSensor3);
		this.sensorTimeSeries.get("distanceSensor4").add(new Millisecond(), sensorData.distanceSensor4);
	}
	
	class DataGenerator extends Timer implements ActionListener {
		private int i=0;
		DataGenerator(int interval) {
			super(interval, null);
			addActionListener(this);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			MDPSensorData sensorData = MDPSensorDemo.this.env.getSensorData();
			MDPSensorDemo.this.updateSensorData(sensorData);
			this.i+=1;
			System.out.println(this.i);
		}
		
	}
	
	public static void main(String[] args) {
		Environment env = new Environment();
		env.setup(false, true);
		MDPSensorDemo sensorDemo = new MDPSensorDemo("MDP Group2 Sensor Demo", env);
		sensorDemo.pack();
		RefineryUtilities.centerFrameOnScreen(sensorDemo);
		sensorDemo.setVisible(true);
		sensorDemo.new DataGenerator(1000).start();
	}
}

