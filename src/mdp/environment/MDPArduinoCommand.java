package mdp.environment;

/**
 * This class is for algorithm to send command to arduino.
 * Such as moving forward, turn left.
 * @author duan
 *
 */
public class MDPArduinoCommand {
	protected MDPArduinoCommandType type;
	protected boolean hasParameter;
	protected int value;
	
	public MDPArduinoCommand(MDPArduinoCommandType type) {
		this.type = type;
	}
	
	public MDPArduinoCommand(MDPArduinoCommandType type, int value) {
		this(type);
		this.value = value;
		this.hasParameter = true;
	}

}