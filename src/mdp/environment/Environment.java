package mdp.environment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.Timer;

import org.json.JSONObject;

public class Environment {
	private MDPAndroid android = null;
	private MDPArduino arduino = null;
	private String socketHost = null;
	private Integer androidSocketPort = null;
	private Integer arduinoSocketPort = null;
	public BlockingQueue<MDPAndroidRequest> androidRequestQueue = null;
	private boolean connectAndroid;
	private boolean connectArduino;
	
	/**
	 * Create an Environment for our MDP robot algorithm.
	 * </br>
	 * Note: Throughout the execution of algorithm, 
	 * only one Environment should be created.
	 * </br>
	 * After created an Envrionment object, must call "setup" method
	 * to connect to android and arduino.
	 */
	public Environment() {
		this.connectAndroid = false;
		this.connectArduino = false;
	}
	
	/**
	 * The setup method.
	 * If don't want to connected to android, can pass false to android parameter.
	 * @param android
	 * @param arduino
	 */
	public void setup(boolean android, boolean arduino) {
		getConfigs();
		this.androidRequestQueue = new LinkedBlockingQueue<MDPAndroidRequest>();
		if(android) {
			this.android = new MDPAndroid(this.androidRequestQueue,
										  this.socketHost,
										  this.androidSocketPort);
			new Thread(this.android).start();
			this.connectAndroid = true;
		}
		if(arduino) {
			this.arduino = new MDPArduino(this.socketHost, this.arduinoSocketPort);
			this.arduino.connect();
			this.connectArduino = true;
		}
	}
	
	/**
	 * To set selfRequestAndroid  to true will generate android map and status request by Environment itself
	 * @param android
	 * @param arduino
	 * @param selfRequestAndroid
	 */
	public void setup(boolean android, boolean arduino, boolean selfRequestAndroid) {
		this.setup(android, arduino);
		this.new AndroidRequestGenerator(1000, "map").start();
		this.new AndroidRequestGenerator(5000, "status").start();
	}
	
	protected class AndroidRequestGenerator extends Timer implements ActionListener {
		private Random random = new Random();
		private String data;
		
		public AndroidRequestGenerator(int delay, String data) {
			super(delay, null);
			addActionListener(this);
			this.data = data;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			MDPAndroidRequest req = new MDPAndroidRequest(this.random.nextInt(), this.data);
			try {
				( (LinkedBlockingQueue<MDPAndroidRequest>) Environment.this.androidRequestQueue ).put(req);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private void getConfigs() {
		InputStream configFile = null;
		try {
			configFile = this.getClass().getResourceAsStream("config.properties");
			Properties properties = new Properties();
			properties.load(configFile);
			this.socketHost = properties.getProperty("sockhost");
			this.androidSocketPort = Integer.parseInt(properties.getProperty("androidsockport"));
			this.arduinoSocketPort = Integer.parseInt(properties.getProperty("arduinosockport"));
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (configFile != null) {
				try {
					configFile.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	public void sendResponseToAndroid(MDPAndroidRequest requset, String data){
		if(!this.connectAndroid) {
			System.err.println("Sorry, you didn't setup android connection."
					+ " Please call .setup(boolean android, boolean arduino)"
					+ " with true passed in as parameter android");
			return;
		}
		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put("id", requset.getId());
		jsonResponse.put("data", data);
		try {
			this.android.sendToAndroid(jsonResponse.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public MDPSensorData getSensorData() {
		if(!this.connectArduino) {
			System.err.println("Sorry, you didn't setup arduino connection."
					+ " Please call .setup(boolean android, boolean arduino)"
					+ " with true passed in as parameter arduino");
			return null;
		}
		return this.arduino.getSensorData();
	}
	
	public void sendCommandToArduino(MDPArduinoCommand command) {
		if(!this.connectArduino) {
			System.err.println("Sorry, you didn't setup arduino connection."
					+ " Please call .setup(boolean android, boolean arduino)"
					+ " with true passed in as parameter arduino");
			return;
		}
		this.arduino.sendCommandToArduino(command);
	}
	
	public static void main(String[] args) {
	}
}
