package mdp.environment;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

class Utility {
	/**
	 * This method is used to construct the data sending out to Android
	 * @param data the data string
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static byte[] constdata(String data) throws UnsupportedEncodingException {
		if(data==null)
			return null;
		byte[] utf8bytes = data.getBytes("UTF-8");
		byte[] result = new byte[5+utf8bytes.length];
		// This probably will not be adequate
		byte[] lengthbytes = new byte[4];
		for(int i=0; i<4; i++){
			lengthbytes[i] = (byte) (utf8bytes.length >>> (i*8));
		}
		System.arraycopy(lengthbytes, 0, result, 0, 4);
		result[4] = Integer.valueOf(0xaa).byteValue();
		System.arraycopy(utf8bytes, 0, result, 5, utf8bytes.length);
		return result;
	}
	
	public static byte[] myRead(InputStream inputSock, int length) throws IOException {
		int len = 0;
		int readlength;
		byte[] ret = new byte[length];
		while(len < length) {
			readlength = inputSock.read(ret, len, length-len);
			if(readlength==0 || readlength==-1)
				throw new IOException("Error read message from android side socket");
			len += readlength;
		}
		return ret;
	}
}
