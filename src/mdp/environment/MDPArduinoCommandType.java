package mdp.environment;

/**
 * Note: the GETSENSORDATA shouldn't be used, it's only intended to be use
 * internally.
 * @author duan
 *
 */
public enum MDPArduinoCommandType {
	ROTATELEFT(2, true),
	ROTATERIGHT(3, true),
	MOVEFORWARD(0, true),
	MOVEFORWARDWITHDISTANCE(5, true),
	STOP(4, false),
	GETSENSORDATA(1, false);
	
	private int value;
	private boolean block;
	
	MDPArduinoCommandType(int value, boolean block) {
		this.value = value;
		this.block = block;
	}
	
	protected int getvalue() {
		return this.value;
	}
	
	protected String getString() {
		return Integer.toString(this.getvalue());
	}
	
	protected boolean isBlock() {
		return this.block;
	}
}
