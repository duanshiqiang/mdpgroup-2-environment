package mdp.environment;

import org.json.JSONObject;

public class MDPAndroidRequest {
	/**
	 * id is useful mainly for requests that require response
	 * for example request map memory data.
	 * So, for simple, can use 0 for all requests that don't need response
	 * (such as commands like stop).
	 * Then for every request for map memeory data, use a different int id.
	 */
	private int id;
	private String data;
	
	public MDPAndroidRequest() {}
	
	public MDPAndroidRequest(int id, String data) {
		this.id = id;
		this.data = data;
	}
	
	public String toJsonString() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("id", this.id);
		jsonObject.put("data", this.data);
		return jsonObject.toString();
	}
	
	public static MDPAndroidRequest parseJsonString(String jsonString) {
		JSONObject jsonObject = new JSONObject(jsonString);
		MDPAndroidRequest ret = new MDPAndroidRequest();
		ret.setId(jsonObject.getInt("id"));
		ret.setData(jsonObject.getString("data"));
		return ret;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
