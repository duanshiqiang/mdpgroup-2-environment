package mdp.environment;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

class MDPAndroid implements Runnable{
	private BlockingQueue<MDPAndroidRequest> requestsQueue;
	private Socket sock;
	private boolean isConnected;
	private String sockHost;
	private int sockPort;
	private OutputStream sockOutputStream;
	private InputStream sockInputStream;
	
	public MDPAndroid(BlockingQueue<MDPAndroidRequest> requestsQueue, String sockHost, int sockPort) {
		this.requestsQueue = requestsQueue;
		this.sockHost = sockHost;
		this.sockPort = sockPort;
		this.isConnected = false;
		this.sock = null;
		this.sockOutputStream = null;
		this.sockInputStream = null;
	}
	
	public void connect() {
		try {
			System.out.println("Trying to connect socket for android communication.");
			this.sock = new Socket(this.sockHost, this.sockPort);
			this.sockOutputStream = this.sock.getOutputStream();
			this.sockInputStream = this.sock.getInputStream();
			this.isConnected = true;
			System.out.println("Connected to socket for android communication.");
		} catch (IOException e) {
			e.printStackTrace();
			this.isConnected = false;
		}
	}
	
	private String myAndroidRead() throws IOException {
		byte[] header = Utility.myRead(this.sockInputStream, 5);
		if(header[4] != Integer.valueOf(0xaa).byteValue()) {
			System.err.print("Received corrupted data from android side, so reconnect.\n");
			this.isConnected = false;
			this.connect();
			return null;
		}
		int length = 0;
		for(int i=3; i>=0; i--) {
			length >>>= 8;
			length |= (header[i] & 0xFF);
		}
		byte[] dataBytes = Utility.myRead(this.sockInputStream, length);
		return new String(dataBytes, "UTF-8");
	}
	
	@Override
	public void run() {
		if(!this.isConnected)
			this.connect();
		while(true) {
			String data = null;
			try {
				data = this.myAndroidRead();
			} catch (IOException e) {
				e.printStackTrace();
				this.isConnected = false;
				//Why I want to sleep for 1 second? I don't know
				try {
				    Thread.sleep(1000);
				} catch (InterruptedException e1) {
				    e1.printStackTrace();
				}
				this.connect();
				continue;
			}
			MDPAndroidRequest req = MDPAndroidRequest.parseJsonString(data);
			try {
				this.requestsQueue.put(req);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void sendToAndroid(String data) throws IOException {
		byte[] bytedata = null;
		try {
			bytedata = Utility.constdata(data);
			if(bytedata == null)
				return;
			// Is this good or the right way? I don't know, haha
			if(!this.isConnected)
				this.connect();
			this.sockOutputStream.write(bytedata);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.print("Send data To android IOError!\n");
			System.out.println("Try reconnect...");
			this.isConnected = false;
			this.connect();
			System.out.println("Reconnected, so retry once...");
			this.sockOutputStream.write(bytedata);
		}
	}
	
	public static void main(String[] args) {
		BlockingQueue<MDPAndroidRequest> q = new LinkedBlockingQueue<MDPAndroidRequest>();
		MDPAndroid an = new MDPAndroid(q, "192.168.2.2", 22222);
		new Thread(an).start();
		while(true) {
			MDPAndroidRequest req;
			try {
				req = q.take();
				System.out.println(String.valueOf(req.getId())+req.getData());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
