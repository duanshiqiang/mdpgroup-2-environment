package mdp.environment;

import java.io.BufferedReader;
import java.io.IOException;

public class MDPSensorData { 
	public int distanceSensor1;
	public int distanceSensor2;
	public int distanceSensor3;
	public int distanceSensor4;
	public int distanceSensor5;
	
	public MDPSensorData() {};
	
	protected static MDPSensorData readFromStream(BufferedReader br) {
		MDPSensorData ret = new MDPSensorData();
		try {
			ret.distanceSensor1 = Integer.parseInt(br.readLine());
			ret.distanceSensor2 = Integer.parseInt(br.readLine());
			ret.distanceSensor3 = Integer.parseInt(br.readLine());
			ret.distanceSensor4 = Integer.parseInt(br.readLine());
			ret.distanceSensor5 = Integer.parseInt(br.readLine());
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return ret;
	}
}