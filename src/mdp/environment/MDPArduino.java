package mdp.environment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

class MDPArduino {
	private Socket sock;
	private boolean isConnected;
	private String sockHost;
	private int sockPort;
	private OutputStream sockOutputStream;
	private InputStream sockInputStream;
	private BufferedReader sockInputBufferedReader;
	
	public MDPArduino(String sockHost, int sockPort) {
		this.sockHost = sockHost;
		this.sockPort = sockPort;
		this.sock = null;
		this.isConnected = false;
		this.sockOutputStream = null;
		this.sockInputStream = null;
		this.sockInputBufferedReader = null;
	}
	
	public void connect() {
		try {
			System.out.println("Trying to connect socket for arduino communication.");
			this.sock = new Socket(this.sockHost, this.sockPort);
			this.sockOutputStream = this.sock.getOutputStream();
			this.sockInputStream = this.sock.getInputStream();
			this.sockInputBufferedReader = new BufferedReader(new InputStreamReader(this.sockInputStream));
			this.isConnected = true;
			System.out.println("Connected to socket for arduino communication.");
		} catch (IOException e) {
			e.printStackTrace();
			this.isConnected = false;
		}
	}
	
	public MDPSensorData getSensorData() {
		if(!this.isConnected)
			this.connect();
		this.sendCommandToArduino(new MDPArduinoCommand(MDPArduinoCommandType.GETSENSORDATA));
		MDPSensorData ret = null;
		ret = MDPSensorData.readFromStream(this.sockInputBufferedReader);
		
		return ret;
	}
	
	private void sendCommandWithoutBlockToArduion(MDPArduinoCommand command) {
		try {
			this.sockOutputStream.write(command.type.getString().getBytes("UTF-8"));
			if(command.hasParameter) {
				this.sockOutputStream.write("\n".getBytes());
				this.sockOutputStream.write(Integer.toString(command.value).getBytes("UTF-8"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendCommandToArduino(MDPArduinoCommand command) {
		this.sendCommandWithoutBlockToArduion(command);
		if(command.type.isBlock()) {
			String ack = null;
			try {
				ack = this.sockInputBufferedReader.readLine();
				if(ack.equals("1"))
					return;
				else
					System.err.println("Didn't receive ack from arduino!");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static void main(String[] args) {
		MDPArduino ad = new MDPArduino("192.168.2.2", 22223);
		ad.connect();
		ad.sendCommandToArduino(new MDPArduinoCommand(MDPArduinoCommandType.MOVEFORWARD));
//		MDPSensorData sd = ad.getSensorData();
//		if(sd==null)
//			System.out.println("Null");
//		else{
//			System.out.println(sd.distanceSensor1);
//			System.out.println(sd.distanceSensor2);
//			System.out.println(sd.distanceSensor3);
//		}
			
	}
}
